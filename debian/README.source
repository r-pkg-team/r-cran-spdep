Explanation for binary files inside source package according to
  http://lists.debian.org/debian-devel/2013/09/msg00332.html

Files: inst/etc/shapes/auckland.*
Documented in man/auckland.Rd as census area units --- CAU. The dataset
 includes the "nb" object auckland.nb of neighbour relations based on
 contiguity, and the "polylist" object auckpolys of polygon boundaries
 for the CAU.

Files: inst/etc/shapes/bhicv.*
Documented in man/bhicv.Rd as data are collected in the Atlas of condition
 indices published  by the Joao Pinheiro Foundation and UNDP.

Files: inst/etc/shapes/boston_tracts.*
Documented in man/boston.Rd contains the Harrison and Rubinfeld (1978)
 data corrected for a few minor errors and augmented with the latitude
 and longitude of the observations

Files: inst/etc/shapes/columbus.*
Documented in man/columbus.Rd as 49 neighbourhoods in Columbus, OH, 1980 data.
 In addition the data set includes a polylist object polys with the boundaries
 of the neighbourhoods, a matrix of polygon centroids coords, and col.gal.nb,
 the neighbours list from an original GAL-format file

Files: inst/etc/shapes/eire.*
Documented in man/eire.Rd:
 The Eire data set has been converted to shapefile format and placed in the
 etc/shapes directory. The initial data objects are now stored as a
 SpatialPolygonsDataFrame object, from which the contiguity neighbour list is
 recreated. For purposes of record, the original data set is retained.
 .
 The eire.df data frame has 26 rows and 9 columns. In addition, polygons of
 the 26 counties are provided as a multipart polylist in eire.polys.utm
 (coordinates in km, projection UTM zone 30). Their centroids are in
 eire.coords.utm. The original Cliff and Ord binary contiguities are in eire.nb.

Files: inst/etc/shapes/sids.*
Documented in man/nc.sids.Rd as North Carolina SIDS data. It contains data
 given in Cressie (1991, pp. 386-9), Cressie and Read (1985) and Cressie and
 Chan (1989) on sudden infant deaths in North Carolina for 1974-78 and 1979-84.
 The data set also contains the neighbour list given by Cressie and Chan (1989)
 omitting self-neighbours (ncCC89.nb), and the neighbour list given by Cressie
 and Read (1985) for contiguities (ncCR85.nb). The data are ordered by county
 ID number, not alphabetically as in the source tables sidspolys is a
 "polylist" object of polygon boundaries, and sidscents is a matrix of their
 centroids.

Files: inst/etc/shapes/wheat.*
Documented in man/wheat.Rd as Mercer and Hall wheat yield data
 based on version in Cressie (1993), p. 455.

Files: data/afcon.rda
       data/columbus.rda
       data/oldcon.rda
       data/nc.sids.rda
 Derived by permission from SpaceStat data files prepared by Luc Anselin
 The data were downloaded from:
   http://www.spacestat.com/data.htm#data
 All data sets are provided as is,
 and no warranties, expressed or implied are made as to the correctness
 and fitness for a purpose. Proper citation of the source of the data in
 published work would be appreciated.

File: data/eire.rda
 Derived from data sets made available by Michael Tiefelsdorf
 based on sources named in man/eire.Rd, and downloaded from:
   http://geog-www.sbs.ohio-state.edu/faculty/tiefelsdorf/GeoStat.htm

File: data/auckland.rda
 is derived from data sets included with INFOMAP
 in Bailey T, Gatrell A (1995) Interactive Spatial Data Analysis, Harlow:
 Longman, and is used with permission of the authors.

File: data/NY_data.rda
 is derived from data sets for Waller, L. and
 C. Gotway (2004) Applied Spatial Statistics for Public Health Data,
 New York: John Wiley and Sons, and is used with permission of the authors.

Files: inst/etc/shapes/NY8_utm18.*
Used in: inst/doc/SpatialFiltering.R inst/doc/nb.R

Files: inst/etc/misc/raw_grass_borders.RData
Used in: inst/doc/CO69.R
  Example data file for documentation


 -- Andreas Tille <tille@debian.org>  Mon, 23 Jun 2014 09:24:05 +0200

